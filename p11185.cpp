#include <cstdio>
#include <iostream>
using namespace std;

int main()
{
    long int n, i=-1;
    while(scanf("%ld", &n), n>=0){
        if(n==0||n==1||n==2){
            printf("%ld\n", n);
            continue;
        }
        int ans[n];
        while(n!=0){
            i++;
            ans[i]=n%3;
            n=n/3;
        }
        while(i>=0){
            printf("%ld", ans[i]);
            i--;
        }

        i=-1;
        cout<<endl;

    }

    return 0;
}
